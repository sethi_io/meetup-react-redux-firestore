import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import { BrowserRouter } from 'react-router-dom';

const render = (
    <BrowserRouter>
    <App />
    </BrowserRouter>
)

ReactDOM.render(render, document.getElementById('root'));
registerServiceWorker();
