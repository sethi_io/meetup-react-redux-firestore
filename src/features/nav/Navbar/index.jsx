import React, { Component, Fragment } from 'react';
import { Menu, Container, Button  } from 'semantic-ui-react';
import { NavLink, Link, withRouter } from 'react-router-dom';
import SignedOutMenu from '../../Menus/SignedOutMenu';
import SignedInMenu from '../../Menus/SignedInMenu';

class Navbar extends Component {
  state = {
    authenticated: false
  }

  handleAuthenticationToggle = () => {
    this.setState({ authenticated: !this.state.authenticated })
    if(this.state.authenticated) return this.props.history.push('/');
  };

  render() {
    const { authenticated } = this.state;
    return (
            <Menu inverted fixed="top">
              <Container>
                <Menu.Item header as={NavLink} to='/'>
                  Meetup
                </Menu.Item>
                <Menu.Item as={NavLink} to='/events' name="Events" />
                { authenticated && (
                <Fragment>
                  <Menu.Item as={NavLink} to='/people' name="People" />
                  <Menu.Item>
                    <Button as={Link} to='/createEvent' floated="right" positive inverted content="Create Event" />
                  </Menu.Item>
                </Fragment>
                )}
              { authenticated ? <SignedInMenu signOut={this.handleAuthenticationToggle} /> : <SignedOutMenu signIn={this.handleAuthenticationToggle} /> }
              </Container>
            </Menu>
    )
  }
}


export default withRouter(Navbar);