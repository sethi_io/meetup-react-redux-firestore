import React from 'react';


const HomePage = ({ history }) => {

  const pushToEvents = () => {
    history.push('/events')
  }

  return (
    <div>
          <div className="ui inverted vertical masthead center aligned segment">
            <div className="ui text container">
              <h1 className="ui inverted stackable header">
                <img
                  className="ui image massive"
                  src="/assets/logo.png"
                  alt="logo"
                />
                <div className="content">Meetup</div>
              </h1>
              <h2>Meet with your clan</h2>
              <div className="ui huge white inverted button" onClick={pushToEvents}>
                Get Started
                <i className="right arrow icon" />
              </div>
            </div>
          </div>
        </div>
  )
}

export default HomePage
